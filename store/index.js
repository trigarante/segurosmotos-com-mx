import Vuex from "vuex";
import cotizarMoto from "~/plugins/cotizacionMoto";
import databasemotos from "~/plugins/saveData";
import getTokenService from "~/plugins/getToken";
import validacionesService from "../plugins/validaciones";
import dbService from "~/plugins/configBase";
import cotizacionPromo from "~/plugins/descuentoService.js";
import telApi from "~/plugins/telefonoService.js";
import saveDataCliente from "~/plugins/saveDataCliente";

const createStore = () => {
  const validacion = new validacionesService();
  return new Vuex.Store({
    state: {
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      idEndPoint: "",
      status: "",
      message: "",
      config: {
        time:'',
        interior1:'',
        loading:true,
        habilitarBtnInteraccion: true,
        aseguradora: "",
        telefonoAS: "",
        grupoCallback: "",
        from: "",
        idPagina: 0,
        idCampana: 0,
        idAseguradora: 51,
        desc:'',
        msi:'',
        promoLabel:'',
        promoImg: '',
        extraMsg: '',
        promoSpecial:false,
        idMedioDifusion: '',
        textoSMS: "Ya tenemos tu cotizacion, en breve un ejecutivo te contactará.",
        btnEmisionDisabled: false,
        interior1:'/',
        statusCot:false,
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        urlOrigen:'',
        aseguradora: '',
        marca: '',
        modelo: '',
        descripcion: '',
        detalle: '',
        clave: '',
        cp: '',
        nombre: '',
        telefono: '',
        gclid_field:'',
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        emailValid:'' ,
        telefonoValid: '',
        codigoPostalValid: '',
        idHubspot:''
    },
   
      solicitud: {},
      cotizacion: {},
      servicios: {
        servicioDB: "http://138.197.128.236:8081/ws-autos/servicios",
      },
    },
    actions: {
      getToken() {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            (resp) => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      saveDataCliente: function (state) {
      let peticionProspecto = {
          nombreUsr:state.formData.nombre,
          telefonoUsr:state.formData.telefono,
          emailUsr:state.formData.correo,
          generoUsr:state.formData.genero,
          codigoPostalUsr:state.formData.cp,
          edadUsr:state.formData.edad,
          respuestaCot: "{}",
          emailValid:state.formData.emailValid === true ? 1 : 0,
          telefonoValid:state.formData.telefonoValid,   
          codigoPostalValid:
           state.formData.codigoPostalValid === true ? 1 : 0,
          idSubRamo:state.config.idSubRamo,
          idMedioDifusion:state.config.idMedioDifusion,
          aseguradora:state.config.aseguradora,
          idPagina:state.config.idPagina,
          telefonoAS:state.config.telefonoAS,
        };
       state.config.dataPeticionCotizacion = peticionProspecto;
        let datosCliente;
       state.formData.telefonoValid = false;
        datosCliente = {
          datosCot: JSON.stringify(state.formData),
          idMedioDifusion:state.config.idMedioDifusion,
          idPagina:state.config.idPagina,
          idSubRamo:state.config.idSubRamo,
          peticionesAli: JSON.stringify(state.config.dataPeticionCotizacion),
        };
        this.commit("enhancedConversionGTAG");
        saveDataCliente
          .search(
            JSON.stringify(datosCliente),
           state.config.accessToken
          )
          .then((resp) => {
            console.log("Registro Datos Cliente - OK ");
            this.$store.state.formData.idLogData = resp.data.id
          })
          .catch((error) => {
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              this.$store
                .dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                   state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                   state.config.accessToken = resp.data.accessToken;
                  }
                  this.saveDataCliente();
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                 state.idEndPoint = 1;
                  this.catchStatusAndMessage(error, {
                    tokenData: process.env.tokenData,
                  });
  
                });
            } else {
             state.idEndPoint = 1;
              this.catchStatusAndMessage(error, datosCliente);
           
            }
          });
          delete(state.formData.urlOrigen);
      },
    enhancedConversionGTAG: function (state) {
        dataLayer.push({
          event: "Lead",
          phone: "52" +state.formData.telefono,
          email:state.formData.correo,
        });
      },
      cotizacionPromo: function (state) {
        cotizacionPromo
          .search(state.config.aseguradora)
          .then(resp => {
            resp = resp.data;
            if(resp.discount != "Sin Registro"){
                state.config.descuento = resp.discount
            }else{
                state.config.descuento = 0
            }

            if (parseInt(resp.delay)) {
              state.config.time = resp.delay
              } else {
              state.config.time = 5000
              }
            }).catch(error=>{
                state.config.descuento = 0
                state.config.time = 5000

            });
        },


    telApi: function (state) {
    try {
      telApi
      .search(state.config.idMedioDifusion)
      .then(resp => { 
        resp = resp.data;
        let telefono = resp.telefono;
        if (parseInt(telefono)) {
          var tel = telefono + "";
          var tel2 = tel.substring(2, tel.length);
          state.config.telefonoAS = tel2;
        }})
      .catch(error => {               
        console.log(error);
      });

        } catch (error) {
            console.log(error);
        }
    },

   
      validarTokenCore: function(state) {
        try {
          if (process.browser) {
            if (
              localStorage.getItem("authToken") === null ||
              localStorage.getItem("authToken") === "undefined"
            ) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
                            * Fecha formateada de unix a fecha normal
                            * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
                            * Se obtiene el tiempo transcurrido en milisegundos
                            * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
                            * Se obtienen las horas de diferencia a partir de la conversión de los
                            * milisegundos a horas.
                            * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
      validateData: function(state) {
        this.commit('saveDataCliente')
        let dataCotizacion = {
          aseguradora: state.config.aseguradora,
          clave: state.formData.clave,
          cp: state.formData.cp,
          descripcion: state.formData.descripcion,
          descuento: state.config.descuento,
          edad: state.formData.edad,
          fechaNacimiento: (
            "01/01/" +
            (new Date().getFullYear() - state.formData.edad).toString()
          ).toString("dd/MM/yyyyy"),
          genero: state.formData.genero === "M" ? "MASCULINO" : "FEMENINO",
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          movimiento: "cotizacion",
          paquete: "AMPLIA",
          servicio: "MOTOS",
        };
        state.config.dataPeticionCotizacion = dataCotizacion;

        validacion
          .validarCorreo(state.formData.correo)
          .then((resp) => {
            if (typeof resp.data == "undefined") {
              state.formData.emailValid = resp.valido;
            } else if (typeof resp.data.valido != "undefined") {
              state.formData.emailValid = resp.data.valido;
            }
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                // state.formData.telefonoValid = resp.data.mensaje
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: "error en el servicio: " + error.response,
                        peticion: state.formData.telefono,
                      })
                    : JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      });
               
                this.commit("validacionCP");
              });
          })
          .catch((error) => {
            state.formData.emailValid = true;
            state.idEndPoint = 157;
            state.status =
              error.response !== undefined ? error.response.status : 500;
            state.message =
              error.response !== undefined
                ? JSON.stringify({
                    error: "error en el servicio: " + error.response,
                    peticion: state.formData.correo,
                  })
                : JSON.stringify({
                    error: "error en el servicio: " + error,
                    peticion: state.formData.correo,
                  });
           
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                // state.formData.telefonoValid = resp.data.mensaje
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      })
                    : JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      });
               
                this.commit("validacionCP");
              });
          });
      },
      validacionCP(state) {
        validacion
          .validarCodigoPostal(
            state.formData.cp,
            state.config.accessToken
          )
          .then((resp) => {
            state.formData.codigoPostalValid =
              typeof resp.data == "object" && resp.data.length > 0
                ? true
                : false;
            if (state.config.cotizacion) {
              this.commit("isGamaAlta");
            } else {
              this.commit("saveDataMoto");
            }
          })
          .catch((error) => {
            console.log(error);
            var status = error.status
              ? error.status
              : error.response.status
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            state.formData.codigoPostalValid = true;
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("validacionCP");
                })
                .catch((error) => {
                  if (state.config.cotizacion) {
                    if (state.config.cotizacion) {
                      this.commit("isGamaAlta");
                    } else {
                      this.commit("saveDataMoto");
                    }
                  }
                });
            } else {
              state.idEndPoint = 1625;
              state.status =
                status !== "ERR_CONNECTION_REFUSED"
                  ? error.response.status
                  : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response,
                      peticion: JSON.parse(error.config.data),
                    })
                  : JSON.stringify({
                      error: error,
                      peticion: state.formData.cp,
                    });
             
              if (state.config.cotizacion) {
                if (state.config.cotizacion) {
                  this.commit("isGamaAlta");
                } else {
                  this.commit("saveDataMoto");
                }
              }
            }
          });
      },
      isGamaAlta: function(state) {
        // let cars = ["BIMOTA", "BMW", "DUCATI", "HARLEY DAVISON", "INDIAN", "MV AGUSTA", "NORTON", "TRIUMPH", "VYRUS"];
        // let isAlta = cars.find(m => {
        //     return m === state.formData.marca
        // });
        // isAlta ? state.formData.paquete = 'AMPLIA' : state.formData.paquete = 'LIMITADA';
        state.formData.paquete = "AMPLIA";
        if (state.config.cotizacion) {
          this.commit("cotizaMoto");
        } else {
          this.commit("saveDataMoto");
        }
      },
      cotizaMoto: function(state) {
        // console.log(dataMotos);
        var dataMotos = {
          aseguradora: state.config.aseguradora,
          clave: state.formData.clave,
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          detalle:state.formData.detalle,
          descripcion: state.formData.descripcion,
          cp: state.formData.cp,
          descuento: state.config.descuento,
          edad: state.formData.edad,
          fechaNacimiento: state.formData.fechaNacimiento,
          genero: state.formData.genero,
          movimiento: "cotizacion",
          paquete: "AMPLIA",
          servicio: "MOTOS",
        };
        cotizarMoto
          .search(
            state.config.aseguradora,
            JSON.stringify(dataMotos),
            state.config.accessToken
          )
          .then((resp) => {
            resp = resp.data;
            state.cotizacion = resp;
            state.formData.precio = state.cotizacion.cotizacion.primaTotal;
            this.commit("valid");
            this.commit("limpiarCoberturas");
            this.commit("saveDataMoto");
          })
          .catch((error) => {
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("cotizaMoto");
                })
                .catch((error) => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 1707;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                          error: error.response.data,
                          peticion: JSON.parse(error.config.data),
                        })
                      : "ERR_CONNECTION_REFUSED";
                 
                  this.commit("saveDataMoto");
                });
            } else {
              state.idEndPoint = 1587;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data),
                    })
                  : "ERR_CONNECTION_REFUSED";

              this.commit("valid");
              this.commit("saveDataMoto");
             
            }
          });
      },
      saveDataMoto: function(state) {
        // state.formData.grupoCallback = state.config.grupoCallback;
        state.formData.dominioCorreo = state.config.dominioCorreo;
        if (state.formData.aseguradora == "GNPMOTOS") {
          state.formData.aseguradora = "MOTOS";
        }
        databasemotos
          .search(
            state.formData.nombre,
            state.formData.telefono,
            state.formData.correo,
            state.formData.genero,
            state.formData.cp,
            state.formData.edad,
            JSON.stringify(state.formData),
            state.config.cotizacion === true
              ? JSON.stringify(state.cotizacion)
              : "{}",
            state.formData.emailValid == true ? 1 : 0,
            state.formData.telefonoValid,
            state.config.textoSMS,
            state.formData.codigoPostalValid == true ? 1 : 0,
            state.config.idSubRamo,
            state.config.idMedioDifusion,
            state.formData.aseguradora,
            state.config.idPagina,
            state.config.ipCliente,
            state.config.telefonoAS,
            state.config.accessToken
          )
          .then((resp) => {
            state.ejecutivo.nombre = resp.data.nombreEjecutivo;
            state.ejecutivo.correo = resp.data.correoEjecutivo;
            this.commit("valid");
          })
          .catch((error) => {
            this.commit("valid");
            console.log("error saveDataMoto", error);
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveDataMoto");
                })
                .catch((error) => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 1707;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                          error: error.response.data,
                          peticion: JSON.parse(error.config.data),
                        })
                      : "ERR_CONNECTION_REFUSED";
                 
                });
            } else {
              state.idEndPoint = 1698;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data),
                    })
                  : "ERR_CONNECTION_REFUSED";
             
            }
          });
      },
      limpiarCoberturas(state) {
        if (state.cotizacion.coberturas) {
          for (var c in state.cotizacion.coberturas[0]) {
            if (state.cotizacion.coberturas[0][c] === "-") {
              delete state.cotizacion.coberturas[0][c];
            }
          }
        }
      },
      valid: function (state) {
        // console.log("Validando datos de la cotizacion");
        if (
          state.formData.precio === "" &&
          state.ejecutivo.id === ""
        ) {
          state.msj = true;
        }
  
  
        if (
          state.formData.precio == 0.0 ||
          state.formData.precio === "" ||
          state.formData.precio <= 1000 ||
          state.cotizacion.cotizacion==null ||
    !state.formData.precio
        ) {
          state.msjEje = true;
          state.config.loading = false;
        } else {
          state.cargandocotizacion = true;
        }
      },
  
    },
  });
};

export default createStore;
