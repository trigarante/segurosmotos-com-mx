module.exports = {
  target:'static',
  /*
   ** Headers of the page
   */
  head: {
    title: "segurosmotos.com.mx",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Motos" },
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' }
    ],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,

    // vendor: ['jquery','bootstrap'],
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          // loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: ['static/css/bootstrap.min.css',"static/css/styles.css"],
  plugins: [
    //'~plugins/bootstrap.js',
    { src: "~/plugins/filters.js", ssr: false },
    { src: '~/plugins/apm-rum.js', ssr: false },
  ],
  modules: 
    [ "@nuxtjs/axios"], /* gtm:{ id: "GTM-KHZSXKZ" }, */ 
  
  env: {
    urlDB: "https://segurosmotos.com.mx/ws-autos/servicios", //segurosmotos
  },
  render: {
    http2: { push: true },
    resourceHints: true,
    compressor: { threshold: 9 },
  },
  hooks: {
    // This hook is called before saving the html to flat file
    "generate:page": (page) => {
      if (/^\/amp\//gi.test(page.route)) {
        page.html = ampify(page.html);
      }
    },
    // This hook is called before serving the html to the browser
    "render:route": (url, page, { req, res }) => {
      if (/^\/amp\//gi.test(url)) {
        page.html = ampify(page.html);
      }
    },
  },
  env: {
    tokenData:'mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=',
    coreBranding: "https://dev.core-brandingservice.com", //CORE
    catalogosGNP:"https://dev.web-gnp.mx", //CATALOGOS GNP
    catalogosQualitas:"https://dev.ws-qualitas.com", //CATALOGOS QUALITAS
    urlValidaciones: "https://core-blacklist-service.com/rest", //PRODUCCIÓN
    urlGetConfiguracionCRM: "https://www.mark-43.net/mark43-service/v1", //PRODUCCIÓN

    urlTactos: "https://ws-camiones.com", //SITIO
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS
    sitio: "https://p.segurosmotos.com.mx/",
    motorCobroGNP: "https://p.gnp-comprasegura.com/cliente",
    motorCobroQUALITAS: "https://p.qualitas-comprasegura.com/cliente",
    Environment:'DEVELOP',
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 }
  }
};
