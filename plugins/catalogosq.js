import axios from 'axios'

// let catalogo = process.env.catalogo + "/catalogos";
let urlConsumo = process.env.catalogosQualitas + '/v3'

class Catalogos {
  marcas() {
    return axios({
      method: "get",
      url: urlConsumo + `/qualitas-motos/brands`,

    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: urlConsumo + `/qualitas-motos/years?brand=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: urlConsumo + `/qualitas-motos/models?brand=${marca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: urlConsumo + `/qualitas-motos/variants?brand=${marca}&model=${submarca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
}

export default Catalogos;
