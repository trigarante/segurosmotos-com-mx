import axios from 'axios'

const getTokenService = {}

getTokenService.search = function () {
    return axios({
        method: "post",
        //token SalesForce
        url: process.env.promoCore + '/v1/authenticate',
        data: {"tokenData": process.env.tokenData}
    })
}
export default getTokenService
