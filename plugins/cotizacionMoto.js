import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (aseguradora,peticion,accessToken) {
 aseguradora= aseguradora.toLowerCase();
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    //COTIZACION SALESFORCE
    url: process.env.promoCore + `/v2/${ aseguradora }/motorcycle/quotation`,
    // url: 'https://core-persistance-service.com/v2/'+`${aseguradora}`+'/quotation',
    data: JSON.parse(peticion)
  })
}
export default cotizacionService
