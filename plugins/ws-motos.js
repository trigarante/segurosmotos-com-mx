import trae from 'trae'

const autosService = trae.create({
  baseUrl: process.env.coreBranding+'/v3',
})

export default autosService